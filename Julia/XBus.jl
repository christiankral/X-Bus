using ElectricalEngineering, ElectricalEngineering.Tab20bc, PyPlot
using Unitful, Unitful.DefaultSymbols
using OMJulia
using DelimitedFiles

# OpenModelica-Simulation wird nur mit modelica_sim = true ausgeführt
modelica_sim = false

# RESULTAT-DATEI
file = "../Results/Results.md"

# UMWELTDATEN
# Erdbeschleunigung
g = 9.81u"m/s^2"
# Referenzwert der Massedichte der Luftdichte
rho_ref = 1.2041u"kg/m^3"
# Referenzwert der Rollreibungskoeffizient
mu_ref = 0.010

# ELEKTRISCHE ANTRIEBS-, BATTERIE- UND FAHRZEUGDATEN
# Mechanische Dauerleistung = Bemessungsleistung
P_N = 15u"kW"
# Nominalenergie der Batterie
E_N = 30u"kW*hr"
# Wirkungsgrad
eta = 0.9

# FAHRZEUG
# Maximale Geschwindigkeit
v_max = 100u"km/hr"
# Masse
m = 600u"kg" + 216u"kg" + 200u"kg"
# Breite
b = 1633u"mm"
# Höhe (m)
h = 2038u"mm"
# Bodenfreiheit (m)
f = 260u"mm"
# Reifenbreite  (m)
w = 185u"mm"

# UNTERSUCHUNGSDATEN SOMMER UND WINTER
# Rollreibungskoeffizient Offroad-Reifen
mu_Offroad = 0.02
# Massedichte der Luftdichte
rho_Sommer = rho_ref
rho_Winter = 1.3163u"kg/m^3"
# Elektrische Grundlast (Klimaanlage + Licht + Grundlast)
PG_Klima = 1200u"W"
PG_Heizung100 = 1000u"W"
PG_Heizung50 = 500u"W"
PG_Grundlast = 200u"W"
# Energie der Batterie auf 80% reduziert
E_80 = 0.8 * E_N

# BERECHNUNG VON PARAMETERN
# Querschnittsfläche (m2)
area = b * (h - f) + 2 * w * f
area_m2 = uconvert(Unitful.NoUnits, area / u"m^2")
printuln("A", area, u"m^2")
# Wirksame Querschnittsfläcje (m2)
cwA = (P_N - mu_ref * m * g * v_max) ./ (rho_ref * v_max^3 / 2)
cwA_m2 = uconvert(Unitful.NoUnits, cwA / u"m^2")
printuln("cw⋅A", cwA, u"m^2")
# Luftwiderstandsbeiwert (1)
cw = cwA / area
cw_1 = uconvert.(Unitful.NoUnits, cw)
printuln("cw",cw_1)

function distance(m, g, mu, cwA, rho, v, E, eta, PG)
    # m = Masse
    # g = Erdbeschleunigung
    # cwA = Luftwiderstandsbeiwert * Querschnittsfläche
    # rho = Rollreibungskoeffizient
    # v = Untersuchte Geschwindigkeit
    # E = Energieinhalt der Batterie
    # eta = Wirkungsgrad Maschine + Wechselrichter
    # PG = Grundlast der elektrischen Leistung
    F = mu * m * g .+ cwA * rho * v.^2 / 2
    P = F .* v
    Pel = P / eta .+ PG
    t = E ./ Pel
    s = v .* t
    # Ermittle Zahlenwerte zu spezifizierten Einheiten für Plot
    Pel_kW = uconvert.(Unitful.NoUnits, Pel./1u"kW")
    t_hr = uconvert.(Unitful.NoUnits, t./1u"hr")
    s_km = uconvert.(Unitful.NoUnits, s./1u"km")
    return Pel, t, s, Pel_kW, t_hr, s_km
end

# SOMMER,

# Geschwindigkeitsvektor
v = collect(0u"km/hr":1u"km/hr":v_max)
v_kmh = uconvert.(Unitful.NoUnits, v./1u"km/hr")
# Luftdichte

close("all")

# SOMMER
figure(figsize=(6.6,5))

# Sommer ohne Klimaanlage
mu = mu_Offroad
rho = rho_Sommer
PG = PG_Grundlast
E = E_N
(Pel, t, s, Pel_kw, t_hr, s_km) = distance(m, g, mu, cwA, rho, v, E, eta, PG)
plot(v_kmh, s_km, color = colorGreen1,
    linestyle = lineStyle1, linewidth = lineWidth1,
    label = "(1)")

# Sommer mit Klimanalage
PG = PG_Grundlast + PG_Klima
(Pel, t, s, Pel_kw, t_hr, s_km) = distance(m, g, mu, cwA, rho, v, E, eta, PG)
plot(v_kmh, s_km, color = colorRed2,
    linestyle = lineStyle1, linewidth = lineWidth2,
    label = "(2)")

# Sommer mit Klimanalage und 80% Batteriekapazität
E = E_80
(Pel, t, s, Pel_kw, t_hr, s_km) = distance(m, g, mu, cwA, rho, v, E, eta, PG)
plot(v_kmh, s_km, color = colorBlack3,
    linestyle = lineStyle3, linewidth = lineWidth3,
    label = "(3)")

xticks(collect(0:20:100))
xlabel(L"v\,(\textrm{km/h})")
yticks(collect(0:100:400))
ylabel(L"s\,(\textrm{km})")
grid(true)
legend(loc="upper right",fontsize=legendFontSize)
savefig("../Results/Sommer.png")

# WINTER
figure(figsize=(6.6,5))

# Winter mit geringer Heizleistung
mu = mu_Offroad
rho = rho_Winter
PG = PG_Grundlast + PG_Heizung50
E = E_N
(Pel, t, s, Pel_kw, t_hr, s_km) = distance(m, g, mu, cwA, rho, v, E, eta, PG)
plot(v_kmh, s_km, color = colorGreen1,
    linestyle = lineStyle1, linewidth = lineWidth1,
    label = "(1)")

# Winter mit voller Heizleistung
PG = PG_Grundlast + PG_Heizung100
(Pel, t, s, Pel_kw, t_hr, s_km) = distance(m, g, mu, cwA, rho, v, E, eta, PG)
plot(v_kmh, s_km, color = colorRed2,
    linestyle = lineStyle1, linewidth = lineWidth2,
    label = "(2)")

# Sommer mit Klimanalage und 80% Batteriekapazität
E = E_80
(Pel, t, s, Pel_kw, t_hr, s_km) = distance(m, g, mu, cwA, rho, v, E, eta, PG)
plot(v_kmh, s_km, color = colorBlack3,
    linestyle = lineStyle3, linewidth = lineWidth3,
    label = "(3)")

xticks(collect(0:20:100))
xlabel(L"v\,(\textrm{km/h})")
yticks(collect(0:100:400))
ylabel(L"s\,(\textrm{km})")
grid(true)
legend(loc="upper right",fontsize=legendFontSize)
savefig("../Results/Winter.png")

# SIMULATION DER SOLARLADUNG
# https://www.openmodelica.org/doc/OpenModelicaUsersGuide/latest/omjulia.html
# Change to work directory
if modelica_sim
    model = OMJulia.OMCSession()
    ModelicaSystem(model,"../Modelica/XBus/package.mo","XBus.Charging")
    simulate(model)
end
res = readdlm("../Modelica/XBus/Resources/energy.txt", ';', skipstart = 1, quotes = false)
# Zeit in der Einheit Tage
time_d = res[1:end-1,1] / (24 * 3600)
# Kummulierte Energie
Esolar_kWh = res[:,2] / 3.6E6
# Energie pro Tag
dEsolar_kWh = Esolar_kWh[2:end] - Esolar_kWh[1:end-1]
E_N_kWh = uconvert(Unitful.NoUnits, E_N / u"kW*hr")
figure(figsize=(6.6,5))
plot(time_d, 100 * dEsolar_kWh / E_N_kWh, color=colorGreen1,
    linestyle = lineStyle1, linewidth = lineWidth1)
mon=cumsum([0,31,28,31,30,31,30,31,31,30,31,30,31])
xticks(mon,["Jan","Feb","Mar","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez","Jan"])
ylabel(L"\textrm{Tages-Solarenergie}\,(\%)")
grid(true)
savefig("../Results/Solar.png")

# RESULTAT-DATEI SCHREiBEN
io = open(file, "w+")
println(io, "# Resultate zur Reichweite")
println(io)
println(io,"[Skript](https://gitlab.com/christiankral/X-Bus/-/blob/main/Julia/XBus.jl) zur numerischen Untersuchung mit [Julia](https://julialang.org): Offroad-Bus")
println(io)
println(io,"![XBus](https://electricbrands.de/xbus/shot/shots/thumbs/1.1.0.0.0.0_front.png)")
println(io)
println(io,"- Lufwiderstandsbeiwert aus folgenden Schätzdaten ermittelt")
println(io,"    - Nennleistung PN bei vmax")
println(io,"    - Luftdichte für Sommer, 20°C")
println(io,"    - Reifenreibung mu = 0.012 (normale Reifen auf Asphalt)")
println(io,"    - Fahrzeugmasse wie geschätzt")
println(io,"- Luftwidertandsbeiwert ⋅ Fläche = cw ⋅ A = ", round(cwA_m2; sigdigits = 4), " m2")
println(io,"- Geschätzte Fläche A = ", round(area_m2; sigdigits = 4), " m2")
println(io,"- Daraus berechneter Luftwiderstandsbeiwert cw = ", round(cw_1; sigdigits = 4))
println(io)
println(io,"## Sommer (20°C)")
println(io)
println(io,"Untersuchung der Reichweite s bei konstanter Geschwindigkeit v")
println(io)
println(io,"1. Grundlast (keine Klimatisierung)")
println(io,"2. Klimatisierung")
println(io,"3. Klimatisierung und 80 % Batteriekapazität")
println(io)
println(io,"![Sommer](./Sommer.png)")
println(io)
println(io,"## Winter (-5°C)")
println(io)
println(io,"Untersuchung der Reichweite s bei konstanter Geschwindigkeit v")
println(io)
println(io,"1. Grundlast + 50 % Heizung")
println(io,"2. Grundlast + 100 % Heizung")
println(io,"3. Grundlast + 100 % Heizung und 80 % Batteriekapazität")
println(io)
println(io,"![Winter](./Winter.png)")
println(io)
println(io,"## Interpretation")
println(io)
println(io,"- Bei der Geschwindigkeit v = 0 ist die Reichweite s = 0, da der Eigenverbrauch die Batterie entleert")
println(io,"- Es gibt immer eine maximale Reichweite als Funktion der Geschwindigkeit")
println(io,"- Der Verbrauch von Heizung und Klimatisierung beeinflusst die Reichweite signifikant")
println(io,"- Der Fall 80 % Batteriekapazität berücksichtigt die voraussichtliche Alterung der Batterie innerhalb von ca. zehn Jahren")
println(io)
println(io,"# Resultate zum solaren Laden")
println(io)
println(io,"Ladeenergie bezogen auf die Batteriekapazität")
println(io)
println(io,"- Für wolkenlose Tage mit maximaler Strahlungsleistung 1000 W/m2")
println(io,"- Solardach ist horizontal ausgerichtet")
println(io,"- Standort Wien, Österreich")
println(io)
println(io,"![Winter](./Solar.png)")
close(io)
