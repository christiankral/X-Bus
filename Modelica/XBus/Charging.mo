within XBus;
model Charging "Simple module supplies DC grid with maximum power tracker"
  extends Modelica.Icons.Example;
  parameter String fileName = Modelica.Utilities.Files.loadResource("modelica://XBus/Resources/energy.txt");
  parameter Integer nsModule = 1 "Number of series connected modules";
  parameter Integer npModule = 4 "Number of parallel connected modules";
  Modelica.Electrical.Analog.Basic.Ground groundDC annotation (
    Placement(visible = true, transformation(origin = {-40, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PhotoVoltaics.Components.SimplePhotoVoltaics.SimplePlantSymmetric module(
    
  T=298.15,moduleData=moduleData, npModule = npModule, nsModule = nsModule,
    useConstantIrradiance=false) annotation (Placement(visible=true, transformation(
        origin={-40,30},
        extent={{-10,10},{10,-10}},
        rotation=-90)));
  PhotoVoltaics.Components.Converters.DCConverter converter annotation (Placement(visible = true, transformation(extent = {{20, 20}, {40, 40}}, rotation = 0)));
  PhotoVoltaics.Components.Blocks.MPTrackerSample mpTracker( ImpRef = npModule * moduleData.ImpRef,VmpRef = nsModule * moduleData.VmpRef,
    samplePeriod=10)                                                                                                annotation (
    Placement(visible = true, transformation(extent = {{0, -40}, {20, -20}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(visible = true, transformation(extent = {{70, -10}, {90, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.PowerSensor powerSensor annotation (
    Placement(visible = true, transformation(origin = {-10, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  PhotoVoltaics.Sources.Irradiance.Irradiance irradiance(startDay = 1, startMonth = 1, startYear = 2022)  annotation (Placement(visible = true, transformation(extent = {{-90, 20}, {-70, 40}}, rotation = 0)));
  parameter PhotoVoltaics.Records.TSM_200_DC01A moduleData annotation(
    Placement(visible = true, transformation(extent = {{-50, 70}, {-30, 90}}, rotation = 0)));
  Modelica.Blocks.Continuous.Integrator integrator(y(unit = "J")) annotation(
    Placement(visible = true, transformation(extent = {{-30, -40}, {-50, -20}}, rotation = 0)));
  Modelica.Blocks.Math.Mean mean(f = 1 / 24 / 3599.99)  annotation(
    Placement(visible = true, transformation(origin = {-70, -70}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = 48)  annotation(
    Placement(visible = true, transformation(origin = {80, 30}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  PhotoVoltaics_TGM.WriteCSV writeCSV(delimiter = ";",fileName = fileName, inputQuantity = "Esolar_J")  annotation(
    Placement(visible = true, transformation(origin = {-70, -30}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(groundDC.p, module.n) annotation(
    Line(points = {{-40, 10}, {-40, 20}}, color = {0, 0, 255}));
  connect(mpTracker.vRef, converter.vDCRef) annotation(
    Line(points = {{21, -30}, {24, -30}, {24, 18}}, color = {0, 0, 127}));
  connect(module.p, powerSensor.pc) annotation(
    Line(points = {{-40, 40}, {-40, 40}, {-40, 50}, {-20, 50}}, color = {0, 0, 255}));
  connect(mpTracker.power, powerSensor.power) annotation(
    Line(points = {{-2, -30}, {-20, -30}, {-20, 39}}, color = {0, 0, 127}));
  connect(powerSensor.pc, powerSensor.pv) annotation(
    Line(points = {{-20, 48}, {-20, 58}, {-10, 58}}, color = {0, 0, 255}));
  connect(powerSensor.nv, groundDC.p) annotation(
    Line(points = {{-10, 40}, {-10, 10}, {-40, 10}}, color = {0, 0, 255}));
  connect(irradiance.irradiance, module.variableIrradiance) annotation(
    Line(points = {{-69, 30}, {-52, 30}}, color = {0, 0, 127}));
  connect(groundDC.p, converter.dc_n1) annotation(
    Line(points = {{-40, 10}, {-16, 10}, {10, 10}, {10, 24}, {20, 24}}, color = {0, 0, 255}));
  connect(converter.dc_p1, powerSensor.nc) annotation(
    Line(points = {{20, 36}, {10, 36}, {10, 50}, {0, 50}}, color = {0, 0, 255}));
  connect(converter.dc_n2, ground.p) annotation(
    Line(points = {{40, 24}, {50, 24}, {50, 10}, {80, 10}}, color = {0, 0, 255}));
  connect(powerSensor.power, integrator.u) annotation(
    Line(points = {{-20, 39}, {-20, -30}, {-28, -30}}, color = {0, 0, 127}));
  connect(powerSensor.power, mean.u) annotation(
    Line(points = {{-20, 39}, {-20, -70}, {-58, -70}}, color = {0, 0, 127}));
  connect(constantVoltage.n, ground.p) annotation(
    Line(points = {{80, 20}, {80, 10}}, color = {0, 0, 255}));
  connect(constantVoltage.p, converter.dc_p2) annotation(
    Line(points = {{80, 40}, {80, 50}, {50, 50}, {50, 36}, {40, 36}}, color = {0, 0, 255}));
  connect(integrator.y, writeCSV.u) annotation(
    Line(points = {{-50, -30}, {-58, -30}}, color = {0, 0, 127}));
  annotation (
    Icon(coordinateSystem(extent = {{-100, -100}, {100, 100}}, preserveAspectRatio = true, initialScale = 0.1, grid = {2, 2})),
    Diagram(coordinateSystem(initialScale = 0.1)),
    experiment(StopTime = 31536000, Interval = 86400, Tolerance = 1e-06, StartTime = 0),
    __OpenModelica_simulationFlags(jacobian = "coloredNumerical", nls = "newton", s = "dassl", lv = "LOG_STATS"));
end Charging;