package XBus
  extends Modelica.Icons.ExamplesPackage;
  
  annotation(
    uses(PhotoVoltaics(version = "2.0.0"), Modelica(version = "4.0.0"), PhotoVoltaics_TGM(version = "2.0.0")));
end XBus;