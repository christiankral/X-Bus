# X-Bus

Einige technische Überlegungen zur Reichweite des X-Bus von [ElectricBrands](https://electricbrands.de/)

![XBus](https://electricbrands.de/xbus/shot/shots/thumbs/1.1.0.0.0.0_front.png)

Konkret soll hier der Offroad-Bus mit Geländereifen untersucht werden.

## Technische Daten

### Herstellerangeaben

Angaben des Herstellers [ElectricBrands](https://electricbrands.de/configurator/) => Technische Daten, Stand 2022-03-06

#### Fahrleistung

- Höchstgeschwindigkeit vmax = 100 km/h
- Spitzendrehmoment: 1200 Nm
- Spitzenleistung: 56 kW
- Dauerleistung: PN = 15 kW
- Batteriekapazität max. E = 30 kWh

#### Geometrie

- Fahrzeugbreite Standard: b = 1633 mm  (Breite im Sinne des Luftwiderstands, da die Offroad-Breite vermutlich nur durch die Radkästen zustande kommt)
- Fahrzeughöhe Offroad: h = 2038 mm
- Bodenfreiheit Offroad: f = 260 mm (im Sinne des Luftwiderstands)
- Reifenbreite: w = 185 mm
- Reifendimension: 185/75 R16 => Reifendurchmesser: d = 684 mm

#### Solares Laden

- Spitzenleistung (Bus): PLmax = 820 W

### Schätzwerte

- Leergewicht: 600 kg Bus (ohne Batterien)
- Batterienmasse: 216 kg für 30 kWh
- Zuladung: 200 kg (zwei Personen + Gepäck)
- Mittlerer Wirkungsgrad, Motor + Wechselrichter: 90%
- Mittlere Klimaleistung (Sommer): 1200 W
- Volle Heizleistung (Winter): 1000 W
- Grundlast inkl Licht: 200 W
- [Rollreibungskoeffizient](https://de.wikipedia.org/wiki/Rollwiderstand#Typische_Rollwiderstandskoeffizienten_cR) Offroad-Reifen: μ = 0.02
- Luftwiderstandsbeiwert: cw = ... siehe [Resultate](./Results/Results.md)
- [Luftdichte Winter](https://de.wikipedia.org/wiki/Luftdichte#Temperaturabh%C3%A4ngigkeit) bei -5 °C: ρW = 1.3163 kg/m3
- [Luftdichte Sommer](https://de.wikipedia.org/wiki/Luftdichte#Temperaturabh%C3%A4ngigkeit) bei +20 °C: ρS = 1.2041 kg/m3
- Für solares Laden werden 4 Stück Solarmodule [Comax TSM 200 DC01A](https://static.trinasolar.com/sites/default/files/Comax_DC01A_Datasheet_Feb13_EN.pdf) berücksichtigt (Spitzenleistung 4 x 200 W)
## Technische Komponenten

### Radnabenmotoren

Ein Custom Type Motor [G2.6 von GEMmotors](https://www.gemmotors.si/products/#gem-g-2-6) hat folgende [Daten](https://www.gemmotors.si/wp-content/uploads/2021/08/gem-motors-g26.pdf), (1/4 des Gesamtfahrzeugs, da Allradantrieb):

- Spitzendrehmoment (20 sec): 300 Nm
- Spitzenleistung (20 sec): 14 kW
- Dauerdrehmoment: 250 Nm
- Dauerleistung: 3.75 kW

# [Resultate](./Results/Results.md)
