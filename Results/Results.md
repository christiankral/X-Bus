# Resultate zur Reichweite

[Skript](https://gitlab.com/christiankral/X-Bus/-/blob/main/Julia/XBus.jl) zur numerischen Untersuchung mit [Julia](https://julialang.org): Offroad-Bus

![XBus](https://electricbrands.de/xbus/shot/shots/thumbs/1.1.0.0.0.0_front.png)

- Lufwiderstandsbeiwert aus folgenden Schätzdaten ermittelt
    - Nennleistung PN bei vmax
    - Luftdichte für Sommer, 20°C
    - Reifenreibung mu = 0.012 (normale Reifen auf Asphalt)
    - Fahrzeugmasse wie geschätzt
- Luftwidertandsbeiwert ⋅ Fläche = cw ⋅ A = 0.9479 m2
- Geschätzte Fläche A = 3.0 m2
- Daraus berechneter Luftwiderstandsbeiwert cw = 0.316

## Sommer (20°C)

Untersuchung der Reichweite s bei konstanter Geschwindigkeit v

1. Grundlast (keine Klimatisierung)
2. Klimatisierung
3. Klimatisierung und 80 % Batteriekapazität

![Sommer](./Sommer.png)

## Winter (-5°C)

Untersuchung der Reichweite s bei konstanter Geschwindigkeit v

1. Grundlast + 50 % Heizung
2. Grundlast + 100 % Heizung
3. Grundlast + 100 % Heizung und 80 % Batteriekapazität

![Winter](./Winter.png)

## Interpretation

- Bei der Geschwindigkeit v = 0 ist die Reichweite s = 0, da der Eigenverbrauch die Batterie entleert
- Es gibt immer eine maximale Reichweite als Funktion der Geschwindigkeit
- Der Verbrauch von Heizung und Klimatisierung beeinflusst die Reichweite signifikant
- Der Fall 80 % Batteriekapazität berücksichtigt die voraussichtliche Alterung der Batterie innerhalb von ca. zehn Jahren

# Resultate zum solaren Laden

Ladeenergie bezogen auf die Batteriekapazität

- Für wolkenlose Tage mit maximaler Strahlungsleistung 1000 W/m2
- Solardach ist horizontal ausgerichtet
- Standort Wien, Österreich

![Winter](./Solar.png)
